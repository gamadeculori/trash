﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimController : MonoBehaviour
{

    Vector3 weaponPos;
    Vector3 mousePos;
    float angle;

    void Update()
    {
        weaponPos = this.transform.position;
        mousePos = Input.mousePosition;
        mousePos.z = 5.23f;
        weaponPos = Camera.main.WorldToScreenPoint(weaponPos);
        mousePos.x = mousePos.x - weaponPos.x;
        mousePos.y = mousePos.y - weaponPos.y;
        angle = Mathf.Atan2(mousePos.y, mousePos.x) * Mathf.Rad2Deg;

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if (Mathf.Abs(angle) > 90)
        {
            player.transform.localScale = new Vector3(-1, 1, 1);
            player.GetComponent<AntMovement>().facingRight = true;
            this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle - 180));
        }
        else
        {
            player.transform.localScale = new Vector3(1, 1, 1);
            player.GetComponent<AntMovement>().facingRight = false;
            this.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
        }
    }
}
