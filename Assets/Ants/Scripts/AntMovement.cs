﻿using UnityEngine;
using System.Collections;

public class AntMovement : MonoBehaviour
{
    public float Speed = 0f;
    private float movex = 0f;
    private Animator animator;
    public GameObject hero;
    public GameObject gun;
    public bool facingRight;
    public float jumpForce = 1000f;
    public bool jump = false;
    bool grounded = true;
    void Awake()
    {
        animator = hero.GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        //Vector2 mouse = Input.mousePosition;
        //float Radians = Mathf.Atan2(mouse.y, mouse.x);
        //gun.transform.eulerAngles = new Vector3(0, 0, Radians * Mathf.Rad2Deg);

    }
    void FixedUpdate()
    {
        if (Input.GetButtonDown("Jump") && grounded == true)
        {
            jump = true;

        }

        movex = Input.GetAxis("Horizontal");
        Vector2 velocitySave = GetComponent<Rigidbody2D>().velocity;
        if (Input.GetAxis("Horizontal") > 0)//&& grounded == true)
        {
            animator.SetBool("Walk", true);
            GetComponent<Rigidbody2D>().velocity = new Vector2(movex * Speed, velocitySave.y);
            facingRight = true;
        }
        if (Input.GetAxis("Horizontal") < 0)// && grounded == true)
        {
            animator.SetBool("Walk", true);
            GetComponent<Rigidbody2D>().velocity = new Vector2(movex * Speed, velocitySave.y);
            facingRight = false;
        }
        if (Input.GetAxis("Horizontal") == 0)
        {
            animator.SetBool("Walk", false);
        }

        if (!grounded && GetComponent<Rigidbody2D>().velocity.y == 0)
        {
            grounded = true;
        }
        if (jump)
        {
            print("Jump");
            animator.SetBool("Jump", true);
            // Add a vertical force to the player.
            GetComponent<Rigidbody2D>().AddForce(Vector3.up * jumpForce);

            // Make sure the player can't jump again until the jump conditions from Update are satisfied.
            jump = false;
            grounded = false;
            animator.SetBool("Jump", false);
        }
    }

}

