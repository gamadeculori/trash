﻿using UnityEngine;
using System.Collections;

public class BackgroundMove : MonoBehaviour {

    public GameObject background;
    float lenght;

    public void Start()
    {
        Vector2 sprite_size = GetComponent<SpriteRenderer>().sprite.rect.size;
        lenght = sprite_size.y;
    }
    // Use this for initialization
    void Update ()
    {
        if (DestroyEnemy.collision)
        {
            Move();
            DestroyEnemy.collision = false;
        }
	}
	
	// Update is called once per frame
	void Move ()
    {
        background.transform.position = new Vector2(background.transform.position.x, background.transform.position.y - lenght/10000);
    }
}
