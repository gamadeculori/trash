﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class CanvasBehavior : MonoBehaviour
{
     public GameObject panelPlay, panelPlayAgain;
    public static bool play = false;
    
	// Use this for initialization
	void Start ()
    {
        if (play != true)
        {
            Time.timeScale = 0.0f;
        }
        else
            Time.timeScale = 1.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Inventary.lives == 0)
        {
            panelPlayAgain.SetActive(true);
            Time.timeScale = 0.0f;
        }

        if (play)
        {
            panelPlay.SetActive(false);
        }
    }

    public void Play()
    {
        panelPlay.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void PlayAgain()
    {
        Inventary.lives = 5;
        Inventary.points = 0;
        restartCurrentScene();
    }

    public void restartCurrentScene()
    {
        play = true;
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
