﻿using UnityEngine;
public class DestroyEnemy : MonoBehaviour
{
    public static bool collision = false;

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "enemies")
        {
            collision = true;
            Inventary.points += 100;
            Destroy(col.gameObject);
             
        }
    }
    
}
