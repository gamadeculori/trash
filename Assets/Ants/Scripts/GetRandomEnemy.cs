﻿using UnityEngine;
using System.Collections;

public class GetRandomEnemy : MonoBehaviour
{

    public GameObject Enemy, random;
    public Sprite[] sprites;
    int nrSprite = 0;
    static public int index;
    public float repeatRate;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Enemies", 1, repeatRate);
    }
    
    public void Enemies()
    {
        GameObject player = GameObject.Find("Hero");
        if (player != null)
        {
            GameObject enemy = (GameObject)Instantiate(Enemy, random.transform.position, Quaternion.identity);
            if (nrSprite < sprites.Length)
            {
                enemy.GetComponent<SpriteRenderer>().sprite = sprites[nrSprite];
                index = nrSprite;
                nrSprite++;
            }
            else
            {
                nrSprite -= sprites.Length - 1;
            }
        }
    }
}
