﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Inventary : MonoBehaviour
{
    public Text heartScore, pointsScore;
    public static int lives = 5;
    public static int points = 0;
   
	// Update is called once per frame
	void Update ()
    {
	    heartScore.text = lives.ToString();
        pointsScore.text = points.ToString();
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "enemies")
            lives--;

    }


}
