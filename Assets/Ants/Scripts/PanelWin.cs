﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System;

public class PanelWin : MonoBehaviour
{
    public GameObject win;
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            win.SetActive(true);
            Time.timeScale = 0.0f;
        }
    }

    public void Play()
    {
        Time.timeScale = 1.0f;
        StartCoroutine(PlayI());
    }

    IEnumerator PlayI()
    {
        print("Courotine");
        Inventary.lives = 5;
        Inventary.points = 0;
        CanvasBehavior.play = false;
        yield return new WaitForSeconds(2);
        int scene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}

