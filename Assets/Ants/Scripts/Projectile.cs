﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
    public Rigidbody2D rocket;              // Prefab of the rocket.
    public GameObject weapon;
    public float speed = 20f;               // The speed the rocket will fire at.
    public float weaponCountdown;


    private float time = 0;
    private AntMovement player;       // Reference to the PlayerControl script.


    void Awake()
    {
        // Setting up the references.
        player = transform.root.GetComponent<AntMovement>();
    }


    void Update()
    {
        time += Time.deltaTime;
        // If the fire button is pressed...
        if (time > weaponCountdown && Input.GetButton("Fire1"))
        {
            time = 0;
            int orientationDiff = (player.GetComponent<AntMovement>().facingRight) ? 180 : 0;

            GetComponent<AudioSource>().Play();

            Vector3 shootDirection;
            shootDirection = Input.mousePosition;
            shootDirection.z = 0.0f;
            shootDirection = Camera.main.ScreenToWorldPoint(shootDirection);
            shootDirection = shootDirection - transform.position;
            //...instantiating the rocket
            Quaternion rotation = weapon.transform.rotation;
            rotation.z += orientationDiff;
            Rigidbody2D bulletInstance = Instantiate(rocket, transform.position, weapon.transform.rotation) as Rigidbody2D;
            bulletInstance.velocity = new Vector2(shootDirection.x * speed, shootDirection.y * speed);
        }
    }
}

